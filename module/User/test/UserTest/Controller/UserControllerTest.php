<?php

namespace UserTest\Controller;

use UserTest\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use User\Controller\UserController;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Stdlib\Parameters;
use PHPUnit_Framework_TestCase;

use User\Model\User;
use User\Model\UserRecord;

class UserControllerTest extends PHPUnit_Framework_TestCase
{
    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();
        $this->controller = new UserController();
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'user'));
        $this->event      = new MvcEvent();
        $config = $serviceManager->get('Config');
        $routerConfig = isset($config['router']) ? $config['router'] : array();
        $router = HttpRouter::factory($routerConfig);

        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);
        
    }
    
    public function testLogin()
    {
        $this->routeMatch->setParam('action', 'login');
        $this->controller->getRequest()
             ->setMethod('POST')
             ->setPost(new Parameters(array('login' => 'admin', 'password' => 'some_hash_1')));
        
        $result = $this->controller->dispatch($this->request);
        $vars = $result->getVariables();
        
        $this->assertTrue($vars['success']);
    }

    /*public function testIndexActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'index');
    
        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
    
        $this->assertEquals(200, $response->getStatusCode());
    }*/

}