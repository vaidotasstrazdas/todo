<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Result;
use Zend\View\Model\ViewModel;

class UserController extends AbstractActionController
{
    
    protected $user;
    
    /**
     *
     * Login User.
     *
     */
    public function loginAction()
    {
        // Initialize necessary adapters
        $sm = $this->getServiceLocator();
        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        
        $login = $this->getRequest()->getPost('login');
        $password = $this->getRequest()->getPost('password');
        if (!$this->getRequest()->isPost() || !$login || !$password) {
            return new JsonModel(array('success' => false));
        }
        
        // Initialize data logic
        $authAdapter = new AuthAdapter(
                                    $dbAdapter,
                                    'user',
                                    'login',
                                    'hash'
                        );
        $authAdapter->setIdentity($login)->setCredential(sha1($password));
        
        // Get service
        $auth = new AuthenticationService();
        
        // Authenticate
        $result = $auth->authenticate($authAdapter);
        
        // Check for success
        $success = false;
        if ($result->getCode() == Result::SUCCESS) {
            $user = $this->getUser()->getUser($result->getIdentity());
            $success = $user->status == 'active';
            
            if ($success) {
                $this->getUser()->setSession($user, 14400);
            }
        }
        $result = new JsonModel(array(
            'success' => $success
        ));

        return $result;
    }
    
    /**
     *
     * Check user login status and get how much time in seconds left for the session to expire.
     *
     */
    public function loginCheckAction()
    {
        $user = $this->getUser();
        if ($user->isLogged()) {
            $user = $user->getSession();
            $result = new JsonModel(array(
                'status' => true,
                'timeLeft' => $user->logoutTime - time()
            ));
            return $result;
        }
        return new JsonModel(array('status' => false));
    }
    
    /**
     *
     * Resume user session by specified number of hours.
     *
     */
    public function resumeSessionAction()
    {
        $user = $this->getUser();
        
        $hours = (int) $this->getRequest()->getPost('hours');
        if (!$this->getRequest()->isPost() || !$hours) {
            return new JsonModel(array('success' => false));
        }
        
        if ($user->isLogged()) {
            $session = $user->getSession();
            $timeLeft = $session->logoutTime - time();
            $user->setSession($session->userRecord, $timeLeft + $hours * 3600);
            return new JsonModel(array('status' => true));
        }
        return new JsonModel(array('status' => false));
    }
    
    /**
     *
     * Logout action.
     *
     */
    public function logoutAction()
    {
        $user = $this->getUser();
        if ($user->isLogged()) {
            $user->removeSession();
        }
        return $this->redirect()->toUrl('/');
    }
    
    /**
     *
     * Data for the session dialog.
     *
     */
    public function timerAction()
    {
        $view = new ViewModel();
        $view->setTerminal(true);
        return $view;
    }
    
    /**
     *
     * Get User Model as a Service.
     *
     */
    public function getUser()
    {
        if (!$this->user) {
            $sm = $this->getServiceLocator();
            $this->user = $sm->get('User\Model\User');
        }
        return $this->user;
    }

}