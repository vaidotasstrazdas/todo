<?php

namespace User\Model;

class UserRecord
{
    
    public $id;
    public $login;
    public $password;
    public $group;
    public $language;
    public $status;

    public function exchangeArray($data)
    {
        $keys = array('id', 'login', 'password', 'group', 'language', 'status');
        foreach ($keys as $key) {
            $this->{$key} = isset($data[$key]) ? $data[$key] : null;
        }
    }

}