<?php

namespace User\Model;

// Zend
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container as SessionContainer;
use Zend\ServiceManager\ServiceLocatorInterface;

// My
use User\Model\UserRecord;
use Group\Model\Group;
use Language\Model\Language;

class User
{
    protected $tableGateway;
    protected $services;


    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getUser($login)
    {
        $rowset = $this->tableGateway->select(array('login' => $login));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $login");
        }
        return $row;
    }

    public function saveUser(UserRecord $user)
    {
        $data = array(
            'login' => $user->login,
            'hash'  => sha1($user->password),
            'group' => $user->group->id,
            'language' => $user->language->id,
            'status' => $user->status
        );

        $this->tableGateway->insert($data);
    }
    
    public function setSession(UserRecord $user, $time)
    {
        $session = $this->getSession();
        $session->userRecord = $user;
        $session->logoutTime = time() + $time;
    }
    
    public function removeSession()
    {
        $session = $this->getSession();
        $session->getManager()->getStorage()->clear('user_info');
    }
    
    public function getSession()
    {
        return new SessionContainer('user_info');
    }
    
    public function isLogged()
    {
        $session = $this->getSession();
        $result = isset($session->userRecord) && isset($session->logoutTime) && $session->logoutTime > time();
        return $result;
    }
    
    public function getRole()
    {
        if ($this->isLogged()) {
            $user = $this->getSession()->userRecord;
            $group = $this->services
                          ->get('Group\Model\Group')
                          ->getGroup($user->group);
            return $group->shortcut;
        }
        return 'guest';
    }

    public function deleteUser($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }

    public function setServiceLocator(ServiceLocatorInterface $locator)
    {
        $this->services = $locator;
    }

}