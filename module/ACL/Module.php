<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ACL;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array();
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'ACL' =>  function ($sm) {
                    $acl = new Acl();
                    $group = $sm->get('Group\Model\Group');
                    $acl->addRole('guest');
                    foreach($group->fetchAll() as $groupRecord) {
                        $acl->addRole($groupRecord->shortcut);
                    }
                    
                    $acl->addResource(new Resource('index'));
                    $acl->addResource(new Resource('user'));
                    $acl->addResource(new Resource('admin'));
                    
                    // TODO: Extend Dynamic Resource Allocation
                    $acl->deny('guest', 'user');
                    $acl->deny('guest', 'admin');
                    $acl->deny('user', 'admin');
                    
                    $acl->allow('guest', 'index');
                    $acl->allow('user', 'user');
                    $acl->allow('admin', 'index');
                    $acl->allow('admin', 'user');
                    $acl->allow('admin', 'admin');
                    
                    
                    return $acl;
                },
            ),
        );
    }
}
