<?php

namespace Group;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

use Group\Model\GroupRecord;
use Group\Model\Group;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Group\Model\Group' =>  function ($sm) {
                    $tableGateway = $sm->get('GroupGateway');
                    $table = new Group($tableGateway);
                    return $table;
                },
                'GroupGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GroupRecord());
                    return new TableGateway('group', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
    
}
