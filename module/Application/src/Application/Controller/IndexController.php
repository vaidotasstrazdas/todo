<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    
    protected $priority;
    protected $work;
    protected $user;
    protected $acl;
    
    /*
     *
     * Application index action.
     *
     */
    public function indexAction()
    {
        $view = new ViewModel();
        $user = $this->getUser();
        $acl = $this->getAcl();
        $role = $user->getRole();
        $this->layout()->setVariable('acl', $acl);
        $this->layout()->setVariable('role', $role);
        
        if ($acl->isAllowed($role, 'user')) {
            $userRecord = $user->getSession()->userRecord;
            $work = $this->getWork();
            $priority = $this->getPriority();
            $view->setVariable('priority', $priority);
            $view->setVariable('user', $userRecord);
            $view->setVariable('today', $work->getToday($userRecord->id));
            $view->setVariable('late', $work->getLate($userRecord->id));
            $view->setVariable('important', $work->getImportant($userRecord->id));
            $view->setTemplate('user/user/index.phtml');
        }
        
        return $view;
    }
    
    public function getPriority()
    {
        if (!$this->priority) {
            $sm = $this->getServiceLocator();
            $this->priority = $sm->get('Work\Model\Priority');
        }
        return $this->priority;
    }
    
    public function getWork()
    {
        if (!$this->work) {
            $sm = $this->getServiceLocator();
            $this->work = $sm->get('Work\Model\Work');
        }
        return $this->work;
    }
    
    public function getUser()
    {
        if (!$this->user) {
            $sm = $this->getServiceLocator();
            $this->user = $sm->get('User\Model\User');
        }
        return $this->user;
    }
    
    public function getAcl()
    {
        if (!$this->acl) {
            $sm = $this->getServiceLocator();
            $this->acl = $sm->get('ACL');
        }
        return $this->acl;
    }
    
}
