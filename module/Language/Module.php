<?php

namespace Language;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

use Language\Model\LanguageRecord;
use Language\Model\Language;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Language\Model\Language' =>  function ($sm) {
                    $tableGateway = $sm->get('LanguageGateway');
                    $table = new Language($tableGateway);
                    return $table;
                },
                'LanguageGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new LanguageRecord());
                    return new TableGateway('language', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
    
}
