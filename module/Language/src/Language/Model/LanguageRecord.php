<?php

namespace Language\Model;

class LanguageRecord
{
    
    public $english_name;
    public $native_name;
    public $shortcut;

    public function exchangeArray($data)
    {
        $keys = array('english_name', 'native_name', 'shortcut');
        foreach ($keys as $key) {
            $this->{$key} = isset($data[$key]) ? $data[$key] : null;
        }
    }

}