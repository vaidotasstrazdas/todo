<?php

namespace Work\Model;

use Exception;

// Zend
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container as SessionContainer;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

// My
use Work\Model\PriorityRecord;

class Priority
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function get($user, $level)
    {
        $user = (int) $user;
        $level = (int) $level;
        $rowset = $this->tableGateway->select(array('user' => $user, 'level' => $level));
        $row = $rowset->current();
        if (!$row) {
            throw new Exception("Could not find row user=$user&level=$level");
        }
        return $row;
    }
    
    public function getList($user)
    {
        $user = (int) $user;
        $rowset = $this->tableGateway->select(function (Select $select) use ($user) {
                                        $select->where->equalTo('user', $user);
                                        $select->order('level DESC');
                                    });
        return $rowset;
    }

    public function setServiceLocator(ServiceLocatorInterface $locator)
    {
        $this->services = $locator;
    }

}