<?php

namespace Work\Model;

use Exception;

// Zend
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container as SessionContainer;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Select;

// My
use Work\Model\WorkRecord;

class Work
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getWork($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new Exception("Could not find row $id");
        }
        return $row;
    }

    public function getToday($userId)
    {
        $userId = (int) $userId;
        $rowset = $this->tableGateway->select(function (Select $select) use ($userId) {
                    $select->where->equalTo('user', $userId)
                                  ->equalTo('completion_date', '0000-00-00')
                                  ->equalTo('deadline_date', date('Y-m-d'));
                    $select->order('title ASC');
                });
        return $rowset;
    }
    
    public function getLate($userId)
    {
        $userId = (int) $userId;
        $rowset = $this->tableGateway->select(function (Select $select) use ($userId) {
                    $select->where->equalTo('user', $userId)
                           ->equalTo('completion_date', '0000-00-00')
                           ->lessThan('deadline_date', date('Y-m-d'));
                    $select->order('deadline_date ASC');
                });
        return $rowset;
    }
    
    public function getImportant($userId)
    {
        $userId = (int) $userId;
        $rowset = $this->tableGateway->select(function (Select $select) use ($userId) {
                    $select->where->equalTo('user', $userId)
                                  ->equalTo('completion_date', '0000-00-00');
                    $select->order('priority DESC')->order('title ASC');
                });
        return $rowset;
    }
    
    public function getCompleted($userId)
    {
        $userId = (int) $userId;
        $query = array('user' => $userId, 'compl' => date('Y-m-d'));
        $rowset = $this->tableGateway->select(function (Select $select) use ($userId) {
                    $select->where->equalTo('user', $userId)
                                  ->notEqualTo('completion_date', '0000-00-00');
                    $select->order('title ASC');
                });
        return $rowset;
    }

    public function saveWork(WorkRecord $work)
    {
        $data = array(
            'user' => $work->user,
            'title' => $work->title,
            'deadline_date' => $work->deadline_date,
            'completion_date' => $work->completion_date,
            'priority' => $work->priority
        );

        if ($work->id > 0) {
            $this->tableGateway->update($data, array('id' => $work->id, 'user' => $work->user));
        } else {
            $this->tableGateway->insert($data);
        }
    }

    public function deleteWork($id, $user)
    {
        $this->tableGateway->delete(array('id' => (int) $id, 'user' => (int) $user));
    }
    
    public function complete($id, $user, $completionDate)
    {
        $data = array(
            'completion_date' => $completionDate,
        );
        $this->tableGateway->update($data, array('id' => $id, 'user' => $user));
    }

    public function setServiceLocator(ServiceLocatorInterface $locator)
    {
        $this->services = $locator;
    }

}