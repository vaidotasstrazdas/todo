<?php

namespace Work\Model;

class WorkRecord
{
    
    public $id;
    public $user;
    public $title;
    public $deadline_date;
    public $completion_date;
    public $priority;

    public function exchangeArray($data)
    {
        $keys = array('id', 'user', 'title', 'deadline_date', 'completion_date', 'priority');
        foreach ($keys as $key) {
            $this->{$key} = isset($data[$key]) ? $data[$key] : null;
        }
    }

}