<?php

namespace Work\Model;

class PriorityRecord
{
    
    public $user;
    public $name;
    public $level;

    public function exchangeArray($data)
    {
        $keys = array('user', 'name', 'level');
        foreach ($keys as $key) {
            $this->{$key} = isset($data[$key]) ? $data[$key] : null;
        }
    }

}