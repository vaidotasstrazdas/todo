<?php


namespace Work\Controller;

use Exception;
use DateTime;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Validator\StringLength;

use Work\Model\WorkRecord;

class WorkController extends AbstractActionController
{
    
    protected $user;
    protected $work;
    protected $acl;
    protected $priority;
    
    public function indexAction()
    {
        $user = $this->getUser();
        $acl = $this->getAcl();
        $role = $user->getRole();
        $work = $this->getWork();
        $this->layout()->setVariable('acl', $acl);
        $this->layout()->setVariable('role', $role);
        
        $view = new ViewModel();
        $view->setVariable('completed', $work->getCompleted($user->getSession()->userRecord->id));
        return $view;
    }
    
    /**
     *
     * New work.
     *
     */
    public function newAction()
    {
        $id = $this->getEvent()->getRouteMatch()->getParam('id');
        $id = $id ? $id : 0;
        
        $view = new ViewModel();
        $user = $this->getUser();
        $priorities = $this->getPriority()->getList($user->getSession()->userRecord->id);
        $view->setVariable('priorities', $priorities);
        $view->setVariable('id', $id);
        if ($id > 0) {
            $view->setVariable('work', $this->getWork()->getWork($id));
        }
        $view->setTerminal(true);
        return $view;
    }
    
    /**
     *
     * Add or Edit work.
     *
     */
    public function addAction()
    {
        $user = $this->getUser()->getSession()->userRecord;
        $priorityService = $this->getPriority();
        $workService = $this->getWork();
        
        if (!$this->getRequest()->isPost()) {
            return new JsonModel(array('success' => false));
        }
        
        $id = $this->getRequest()->getPost('id');
        $title = $this->getRequest()->getPost('title');
        $deadlineDate = $this->getRequest()->getPost('deadline_date');
        $completionDate = $this->getRequest()->getPost('completion_date');
        $priority = $this->getRequest()->getPost('priority');
        
        $errors = array();
        if (strlen($title) < 1 || strlen($title) > 100) {
            $errors['title'] = 'Darbo pavadinimo ilgis turi būti nuo 1 iki 100 simbolių.';
        }
        
        $date = DateTime::createFromFormat('Y-m-d', $deadlineDate);
        if (!$date || $date->format('Y-m-d') != $deadlineDate) {
            $errors['deadline_date'] = 'Blogai nurodyta, iki kada reikia atlikti darbą, data.';
        }
        
        if ($completionDate) {
            $date = DateTime::createFromFormat('Y-m-d', $completionDate);
            if (!$date || $date->format('Y-m-d') != $completionDate) {
                $errors['completion_date'] = 'Blogai nurodyta darbo atlikimo data.';
            }
        }
        
        try {
            $priorityService->get($user->id, $priority);
        } catch (Exception $e) {
            $errors['priority'] = 'Blogai nurodytas darbo prioritetas.';
        }
        
        if ($errors) {
            return new JsonModel(array('success' => false, 'errors' => $errors));
        }
        
        $work = new WorkRecord();
        $work->id = $id;
        $work->user = $user->id;
        $work->title = $title;
        $work->deadline_date = $deadlineDate;
        $work->completion_date = $completionDate;
        $work->priority = $priority;
        $workService->saveWork($work);
        
        return new JsonModel(array('success' => true));
    }
    
    /**
     *
     * Delete Work.
     * 
     */
    public function deleteAction()
    {
        $user = $this->getUser();
        $userRecord = $user->getSession()->userRecord;
        $priorityService = $this->getPriority();
        $workService = $this->getWork();
        
        if (!$user->isLogged() || !$this->getRequest()->isPost()) {
            return new JsonModel(array('success' => false));
        }
        
        $id = $this->getRequest()->getPost('id');
        $workService->deleteWork($id, $userRecord->id);
        return new JsonModel(array('success' => true));
    }
    
    /**
     *
     * Complete Work View.
     *
     */
    public function completeViewAction()
    {
        $view = new ViewModel();
        $user = $this->getUser();
        $priorities = $this->getPriority()->getList($user->getSession()->userRecord->id);
        $view->setVariable('priorities', $priorities);
        $view->setTerminal(true);
        return $view;
    }
    
    /**
     *
     * Complete Work.
     *
     */
    public function completeAction()
    {
        $user = $this->getUser();
        $userRecord = $user->getSession()->userRecord;
        $workService = $this->getWork();
        
        if (!$user->isLogged() || !$this->getRequest()->isPost()) {
            return new JsonModel(array('success' => false));
        }
        
        $id = $this->getRequest()->getPost('id');
        $completionDate = $this->getRequest()->getPost('completion_date');
        if ($completionDate) {
            $date = DateTime::createFromFormat('Y-m-d', $completionDate);
            if (!$date || $date->format('Y-m-d') != $completionDate) {
                return new JsonModel(array('success' => false));
            }
        }
        
        $workService->complete($id, $userRecord->id, $completionDate);
        return new JsonModel(array('success' => true));
    }
    
    public function getPriority()
    {
        if (!$this->priority) {
            $sm = $this->getServiceLocator();
            $this->priority = $sm->get('Work\Model\Priority');
        }
        return $this->priority;
    }
    
    public function getWork()
    {
        if (!$this->work) {
            $sm = $this->getServiceLocator();
            $this->work = $sm->get('Work\Model\Work');
        }
        return $this->work;
    }
    
    public function getUser()
    {
        if (!$this->user) {
            $sm = $this->getServiceLocator();
            $this->user = $sm->get('User\Model\User');
        }
        return $this->user;
    }
    
    public function getAcl()
    {
        if (!$this->acl) {
            $sm = $this->getServiceLocator();
            $this->acl = $sm->get('ACL');
        }
        return $this->acl;
    }
    
}
