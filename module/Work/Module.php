<?php

namespace Work;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Work\Model\PriorityRecord;
use Work\Model\Priority;
use Work\Model\WorkRecord;
use Work\Model\Work;


class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Work\Model\Priority' =>  function ($sm) {
                    $tableGateway = $sm->get('PriorityGateway');
                    $table = new Priority($tableGateway);
                    $table->setServiceLocator($sm);
                    return $table;
                },
                'PriorityGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PriorityRecord());
                    return new TableGateway('priority', $dbAdapter, null, $resultSetPrototype);
                },
                'Work\Model\Work' =>  function ($sm) {
                    $tableGateway = $sm->get('WorkGateway');
                    $table = new Work($tableGateway);
                    $table->setServiceLocator($sm);
                    return $table;
                },
                'WorkGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new WorkRecord());
                    return new TableGateway('work', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }

    
}
