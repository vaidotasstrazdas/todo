<?php


namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class AdminController extends AbstractActionController
{
    
    protected $user;
    protected $acl;
    protected $priority;
    
    public function indexAction()
    {
        $user = $this->getUser();
        $acl = $this->getAcl();
        $role = $user->getRole();
        $this->layout()->setVariable('acl', $acl);
        $this->layout()->setVariable('role', $role);
        $view = new ViewModel();
        if (!$acl->isAllowed($role, 'admin')) {
            $this->getResponse()->setStatusCode(403);
            $view->setTemplate('admin/admin/restricted');
            return $view;
        }
        return $view;
    }
    
    public function getPriority()
    {
        if (!$this->priority) {
            $sm = $this->getServiceLocator();
            $this->priority = $sm->get('Work\Model\Priority');
        }
        return $this->priority;
    }
    
    public function getUser()
    {
        if (!$this->user) {
            $sm = $this->getServiceLocator();
            $this->user = $sm->get('User\Model\User');
        }
        return $this->user;
    }
    
    public function getAcl()
    {
        if (!$this->acl) {
            $sm = $this->getServiceLocator();
            $this->acl = $sm->get('ACL');
        }
        return $this->acl;
    }
    
}
