define([
    "dojo",
    "dojo/dom",
    "dojo/request"
], function(dojo, dom, request){
    return {
        /**
        * Login action.
        */
        login: function (login, password) {
            request.post("user/login", {
                handleAs: "json",
                data: {
                    login: login,
                    password: password
                }
            }).then(
                function(response){
                    loginErrorNode = dom.byId("login-error");
                    loginErrorTextNode = dom.byId("login-error-text");
                    if (response.success == false) {
                        dojo.replaceClass(loginErrorNode, "has-error", "has-success");
                        loginErrorTextNode.innerHTML = "Klaidingi prisijungimo duomenys.";
                    } else {
                        dojo.replaceClass(loginErrorNode, "has-success", "has-error");
                        loginErrorTextNode.innerHTML = "Prisijungmo duomenys geri. Tuojau būsite prijungtas.";
                        window.location.reload();
                    }
                },
                function(error) {
                    console.log("The server returned error: ", error);
                }
            );
        },
        
        /**
        * Check login for session expiration information.
        */
        checkLogin: function (interval) {
            var self = this;
            request.post("user/loginCheck", {
                handleAs: "json"
            }).then(
                function(response){
                    if (response.status) {
                        if (response.timeLeft <= 900) {
                            clearInterval(interval);
                            BootstrapDialog.show({
                                closable: false,
                                title: 'Sesija artėja į pabaigą',
                                message: $('<div></div>').load('user/timer'),
                                buttons: [{
                                    label: 'Pratęsti sesiją',
                                    action: function(dialog) {
                                        hours = dom.byId('sessionHours').value;
                                        self.resumeSession(hours, dialog);
                                    }
                                }, {
                                    label: 'Atsijungti',
                                    action: function(dialog) {
                                        window.location.href = 'user/logout';
                                    }
                                }],
                                onshown: function (dialog) {
                                    $('#countdown').countdown({
                                        until: response.timeLeft,
                                        format: 'MS',
                                        layout: '{mnn}:{snn}'
                                    });
                                }
                            });
                        }
                    }
                },
                function(error) {
                    console.log("The server returned error: ", error);
                }
            );
        },
        
        /**
        * Resume User Session.
        */
        resumeSession: function (hours, dialog) {
            var self = this;
            request.post("user/resumeSession", {
                handleAs: "json",
                data: {hours: hours}
            }).then(
                function(response){
                    dialog.close();
                    if (response.status) {
                        BootstrapDialog.show({
                            closable: false,
                            title: 'Sesija sėkmingai pratęsta!',
                            message: 'Jūs sesiją sėkmingai pratęsėte.',
                            buttons: [{
                                label: 'Gerai',
                                action: function(dialog) {
                                    dialog.close();
                                    
                                    // Reset the interval
                                    var interval = setInterval(function () {
                                        self.checkLogin(interval);
                                    }, 5000);
                                }
                            }]
                        });
                    } else {
                        console.log("The server returned this response: ", response);
                    }
                },
                function(error){
                    console.log("The server returned error: ", error);
                }
            );
        }
    };
});