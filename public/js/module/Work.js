define([
    "dojo",
    "dojo/dom",
    "dojo/on",
    "dojo/request"
], function(dojo, dom, on, request){
    return {
        /**
        * Add work.
        */
        addWork: function () {
            var self = this;
            BootstrapDialog.show({
                title: 'Pridėti darbą',
                message: $('<div></div>').load('work/new'),
                buttons: [{
                    label: 'Pridėti darbą',
                    action: function(dialog) {
                        self.insertResource(0);
                    }
                }],
                onshown: function (dialog) {
                    var pattern = /^\d{4}-\d{2}-\d{2}$/
                    deadlineNode = dom.byId("work-deadline_date");
                    completionGroup = dom.byId("work-completion_date-group");
                    
                    on(deadlineNode, "keyup", function(){
                        date = deadlineNode.value;
                        if (date.match(pattern)) {
                            if (new Date(date) < new Date(DATE_TODAY)) {
                                dojo.replaceClass(completionGroup, "show", "hidden");
                            }
                        }
                    });
                }
            });
        },
        
        /**
         *
         * Delete Work.
         *
         */
        deleteWork: function (id) {
            request.post("work/delete", {
                handleAs: "json",
                data: {
                    id: id
                }
            }).then(
                function(response){
                    if (response.success) {
                        window.location.reload();
                    } else {
                        console.log("Work has not been deleted: ", response);
                    }
                },
                function(error) {
                    console.log("The server returned error: ", error);
                }
            );
        },
        
        
        /**
         *
         * Complete Work.
         *
         */
        completeWork: function (id) {
            var self = this;
            BootstrapDialog.show({
                title: 'Užbaigti darbą',
                message: $('<div></div>').load('work/completeView'),
                buttons: [{
                    label: 'Užbaigti',
                    action: function(dialog) {
                        self.complete(id);
                    }
                }]
            });
        },
        
        complete: function (id) {
            completionDate = dom.byId("work-completion_date").value;
            request.post("work/complete", {
                handleAs: "json",
                data: {
                    id: id,
                    completion_date: completionDate
                }
            }).then(
                function(response){
                    if (response.success) {
                        window.location.reload();
                    } else {
                        groupNode = dom.byId("work-completion_date-group");
                        errorNode = dom.byId("work-completion_date-error");
                        dojo.addClass(groupNode, "has-error");
                        errorNode.innerHTML = "Blogai įvesta darbo atlikimo data.";
                    }
                },
                function(error) {
                    console.log("The server returned error: ", error);
                }
            );
        },
        
        editWork: function (id) {
            var self = this;
            BootstrapDialog.show({
                title: 'Redaguoti darbą',
                message: $('<div></div>').load('work/new/' + id),
                buttons: [{
                    label: 'Redaguoti darbą',
                    action: function(dialog) {
                        self.insertResource(id);
                    }
                }]
            });
        },
        
        insertResource: function (edit) {
            title = dom.byId("work-title").value;
            deadlineDate = dom.byId("work-deadline_date").value;
            completionDate = dom.byId("work-completion_date").value;
            priority = dom.byId("work-priority").value;
            request.post("work/add", {
                handleAs: "json",
                data: {
                    id: edit,
                    title: title,
                    deadline_date: deadlineDate,
                    completion_date: completionDate,
                    priority: priority
                }
            }).then(
                function(response){
                    groups = ["title", "deadline_date", "completion_date", "priority"];
                    for (id in groups) {
                        group = groups[id];
                        groupNode = dom.byId("work-" + group + "-group");
                        errorNode = dom.byId("work-" + group + "-error");
                        errorNode.innerHTML = '';
                        dojo.removeClass(groupNode, "has-error");
                    }
                    if (response.success) {
                        window.location.reload();
                    } else {
                        for (group in response.errors) {
                            errorText = response.errors[group];
                            groupNode = dom.byId("work-" + group + "-group");
                            errorNode = dom.byId("work-" + group + "-error");
                            dojo.addClass(groupNode, "has-error");
                            errorNode.innerHTML = errorText;
                        }
                    }
                },
                function(error) {
                    console.log("The server returned error: ", error);
                }
            );
        }
        
    };
});