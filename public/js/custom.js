var dojoConfig = {
    baseUrl: "/js/",
    tlmSiblingOfDojo: false,
    packages: [{
        name: "module",
        location: "module"
    }]
};

require([
    "dojo/dom",
    "dojo/query",
    "dojo/dom-attr",
    "dojo/on",
    "dojo/module/User",
    "dojo/module/Work",
    "dojo/domReady!"
], function(dom, query, domAttr, on, User, Work) {
    // Login functionality
    var loginButtonNode = dom.byId("login-button");
    if (loginButtonNode) {
        on(loginButtonNode, "click", function(){
            login = dom.byId("login").value;
            password = dom.byId("password").value;
            User.login(login, password);
        });
    }
    var interval = setInterval(function () {
        User.checkLogin(interval);
    }, 5000);
    
    // Work functionality
    var addWorkButtonNode = dom.byId("add-work");
    if (addWorkButtonNode) {
        on(addWorkButtonNode, "click", function(){
            Work.addWork();
        });
    }
    
    query(".delete-work").on("click", function () {
        var id = domAttr.get(this, '_id');
        BootstrapDialog.show({
            title: 'Patvirtinkite savo veiksmą',
            message: 'Ar tikrai norite ištrinti šį darbą?',
            closable: false,
            buttons: [{
                label: 'Taip',
                action: function(dialog) {
                    Work.deleteWork(id);
                }
            },
            {
                label: 'Ne',
                action: function(dialog) {
                    dialog.close();
                }
            }]
        });
    });
    
    query(".complete-work").on("click", function () {
        var id = domAttr.get(this, '_id');
        Work.completeWork(id);
    });

    query(".edit-work").on("click", function () {
        var id = domAttr.get(this, '_id');
        Work.editWork(id);
    });
    
});